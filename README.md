# Simplified modeling of corona epidemic
**Just to get an idea of the dynamics**


https://bitbucket.org/rentier18/epidemic/src/master/epidemic/resources/snapshot.png


https://bitbucket.org/rentier18/epidemic/src/master/epidemic/resources/plot.png



* Population is in a square grid.

* Process:
    * everyone contacts, daily, random people in the neighborhood
    * in random contact distance in the grid; normally distributed, rounded
    * Infection occurs at every n-th contact with an infectious person
        
* States / Sequence of disease:
    * healthy,
    * infected, incubation (3 days),
    * infectious, unnoticed, contacts like healthy (3 days),
    * sick (and infectious; less critical, because of attentive care) (6 days),
    * recovered and immune (after 12 days)

