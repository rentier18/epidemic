'''                    
Simplified modeling of corona epidemic
--------------------------------------
Just to get an idea of the dynamics

Population is in a square grid.

Process: 
    - everyone contacts daily random people in the neighborhood
        - in random distance in the grid; normally distributed, rounded
        - Infection occurs at every n-th contact with an infectious person
        
States / Sequence of disease:
    - healthy,
    - infected, incubation (3 days),
    - infectious, unnoticed, contacts like healthy (3 days),
    - sick (and infectious; less critical, because careful handling) (6 days),
    - recovered, immune (after 12 days)                    
'''

import numpy as np
import random
import matplotlib.pyplot as plt
from PIL import Image
from PIL import ImageColor
from PIL import ImageFont
from PIL import ImageDraw 
import os
from datetime import datetime
import matplotlib.ticker as ticker
from enum import Enum

##### SETUP
sDir = "c:\\temp\\corona\\"

size = 1000   #1000**2 = 1Mio population
# seed = 3
days = int(365 * 2)  # max duration

scale = 80  # (80mio for DE) 

contactsWidth = 4
contactsFrq = 3
infProb_I = 5  # infection probability from unnoticed infectious person

contactsWidthSick = 1
contactsFrqSick = 1
infProb_s = 20

framerate = 10
movieSize = 500
###### END SETUP

os.makedirs(sDir, exist_ok=True)

img = Image.new('RGB', (size, size), (0, 0, 0))
imgLegend = Image.open(sDir + 'legend.png')
sizeM = int(size / 2)

State = Enum('State', 'h i I s r')  # healthy, infected, infectious, sick, recovered/immune


class StateT ():  # times; duration in state, days 
    h = None
    i = 3
    I = 3
    s = 6
    r = None


class StateCol ():
    h = ImageColor.getcolor("black", "RGB")
    i = ImageColor.getcolor("yellow", "RGB") 
    I = ImageColor.getcolor("red", "RGB")
    s = ImageColor.getcolor("orange", "RGB") 
    r = ImageColor.getcolor("green", "RGB")


class Corona():
    
    def rndInfection(self): 
        found = False
        while found == False:
            x = random.randint(1, size - 1)
            y = random.randint(1, size - 1)
            if self.population[x][y].state == State.h:
                self.population[x][y].state = State.i
                img.putpixel((x, y), StateCol.i)
                found = True
                
    def seed(self):

        self.population[sizeM][sizeM].state = State.i
        img.putpixel((sizeM, sizeM), StateCol.i)
#    
#         self.population[int(size * 1 / 3)][int(size * 1 /3)].state = 'i'
#         self.population[int(size * 2 / 3)][int(size * 1 / 3)].state = 'i'
#         self.population[int(size * 1 / 3)][int(size * 2 / 3)].state = 'i'
#         self.population[int(size * 2 / 3)][int(size * 2 / 3)].state = 'i'

#         for _ in range(0, seed):
#             self.rndInfection()

    def __init__(self):
        self.day = 0
        self.population = [[Person(i, j) for j in range(size)] for i in range(size)]
        self.cnt = size ** 2
        self.seed()
        self.stateCnt = {}
        self.stateCntDaily = []
        self.timeline = []
        self.id = datetime.now().strftime("%m-%d_%H-%M")
        
        self.imgTmpSize = 50  # initial zoom into population 
        
#         os.makedirs(sDir, exist_ok=True)
        os.chdir(sDir)

    def run(self):
        os.system("del /Q img*.png")

        for self.day in range(1, days):
            
            if self.day % 7 == 0:
                self.rndInfection()
                
            self.nextDay(self.day)
            self.mkImg()
            if (self.stateCnt[State.i] + self.stateCnt[State.I] + self.stateCnt[State.s]) < self.cnt * 0.0025 and self.stateCnt[State.r] >= self.stateCnt[State.h]:
                break
            
#        have a look at some peoples
        for p in range(5):
            print("Person ", self.population[sizeM + p][sizeM].ix, self.population[sizeM + p][sizeM].iy, self.population[sizeM + p][sizeM].t)   
    
        self.mkPlot(self.id)
        mkMovie(self.id)

    def perc2pop(self, x):   
        return x * scale

    def pop2perc(self, x):   
        return x / scale

    def mkImg(self):
        size_ = 50 + int(self.day / 60) * 100  # zoom every xxx days
        if self.imgTmpSize < size_:
            self.imgTmpSize += 5  # smooth zoom
        if self.imgTmpSize < sizeM:  
            imgZoom = img.crop((sizeM - self.imgTmpSize, sizeM - self.imgTmpSize, sizeM + self.imgTmpSize, sizeM + self.imgTmpSize))
            img_ = imgZoom.resize((movieSize, movieSize), Image.ANTIALIAS)
        else:
            img_ = img.resize((movieSize, movieSize), Image.ANTIALIAS)
            
        img2 = concatImg(imgLegend, img_)    
        font = ImageFont.truetype("arial.ttf", 20)
        ImageDraw.Draw(img2).text((50, 5), "day " + str(self.day), (0, 0, 0), font=font)
        imgFile = sDir + 'img' + f'{self.day:03}' + ".png"
        img2.save(imgFile, "PNG")
        
    def mkPlot(self, name="plot"):
        x_day = []
        y_h = []
        y_i = []
        y_I = []
        y_s = []
        y_r = []
        y_newSick = []
        
        for d in self.timeline:
            x_day.append(d[0])
            perc = 1. / size ** 2
            y_h.append(d[1][State.h] * perc)
            y_i.append(d[1][State.i] * perc)
            y_I.append(d[1][State.I] * perc)
            y_s.append(d[1][State.s] * perc)
            y_r.append(d[1][State.r] * perc)
            y_newSick.append(d[2] * perc)
            
        fig, ax = plt.subplots(3)
        ax[0].grid()
        ax[0].secondary_yaxis('right', functions=(self.perc2pop, self.pop2perc))
        ax[0].yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1.,decimals=0))
        ax[0].plot(x_day, y_h, color='black')
        ax[0].plot(x_day, y_r, color='lightgreen')

        ax[1].grid()
#        ax[1].get_xaxis().set_visible(False)
        ax[1].yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1.,decimals=1))
        ax[1].plot(x_day, y_i, color='yellow')
        ax[1].plot(x_day, y_I, color='red')
        ax[1].plot(x_day, y_s, color='orange')
        
        ax1s=ax[1].secondary_yaxis('right', functions=(self.perc2pop, self.pop2perc))
        ax1s.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.0f}'))

        ax[2].grid()
#        ax[2].get_xaxis().set_visible(False)
        ax[2].secondary_yaxis('right', functions=(self.perc2pop, self.pop2perc))
        ax[2].yaxis.set_major_formatter(ticker.PercentFormatter(xmax=1.,decimals=1))
        ax[2].plot(x_day, y_newSick, color='blue')
 
        ax2s=ax[2].secondary_yaxis('right', functions=(self.perc2pop, self.pop2perc))
        ax2s.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.3f}'))

#        plt.show()
        try:
            file = sDir + name + "p.png"
            plt.savefig(file)
            im = Image.open(file)
            im = im.resize((movieSize, movieSize), Image.ANTIALIAS)
            im.save(file)
        except Exception as e:
            print(e)
        plt.close('all')

    def nextDay(self, iDay):
        self.stateCnt = { State.h:0,
                     State.i:0,
                     State.I:0,
                     State.s:0,
                     State.r:0, }
        newSick = 0
        for i in range(0, size):
            for j in range(0, size):
                if self.population[i][j].state != State.r:
                    nighborX, nighborY = self.population[i][j].getRndNighborIdx()
                    self.population[i][j].contact(self.population[nighborX][nighborY], iDay)
                    newSick += self.population[i][j].nextDayState(iDay)
                self.stateCnt[ self.population[i][j].state] += 1
        
        self.timeline.append([iDay, self.stateCnt, newSick])

        print("day ", iDay, "states ", self.stateCnt, "newSick=",newSick)


class Person():

    def nextDayState(self, day):  # return 1 if new sick
        ret = 0
        if self.state == State.h or self.state == State.r:
            pass
#        print ("state bevor ", self.state, self.ix, self.iy, day, self.t)
        elif self.state == State.i and day - self.t["i"] >= StateT.i:
            self.state = State.I
            self.t["I"] = day
            img.putpixel((self.ix, self.iy), StateCol.I)
        elif self.state == State.I and day - self.t["I"] >= StateT.I:
            self.state = State.s
            self.t["s"] = day
            img.putpixel((self.ix, self.iy), StateCol.s)
            ret = 1
        elif self.state == State.s and day - self.t["s"] >= StateT.s:
            self.state = State.r
            self.t["r"] = day
            img.putpixel((self.ix, self.iy), StateCol.r)
#        print ("state after ", self.state, self.ix, self.iy, day, self.t)
        return ret

    def contact(self, person, t):
        # get infected
        if self.state == State.h:
            if person.state == State.I:
                if random.randint(1, infProb_I) == 1:
#                    print("got it! from", person.ix, person.iy, person.state,)
                    self.state = State.i
                    self.t["i"] = t
                    img.putpixel((self.ix, self.iy), StateCol.i)
                    
            if person.state == State.s:
                if random.randint(1, infProb_s) == 1:
#                    print(self.ix, self.iy, " got it! from", person.ix, person.iy, person.state,)
                    self.state = State.i
                    self.t["i"] = t
                    img.putpixel((self.ix, self.iy), StateCol.i)
                    
        # infect other                    
        if self.state == State.I:
            if person.state == State.h:
                if random.randint(1, infProb_I) == 1:
#                    print(self.ix, self.iy, "gave it! to", person.ix, person.iy, person.state,)
                    person.state = State.i
                    person.t["i"] = t
                    img.putpixel((person.ix, person.iy), StateCol.i)
                    
        if self.state == State.s:
            if person.state == State.h:
                if random.randint(1, infProb_s) == 1:
#                    print("gave it! to", person.ix, person.iy, person.state,)
                    person.state = State.i
                    person.t["i"] = t
                    img.putpixel((person.ix, person.iy), StateCol.i)
                
    def getRndNighborIdx(self):
        
        dNighborX = 0
        dNighborY = 0
        while [dNighborX, dNighborY] == [0, 0]:  # until not self
            if self.state == "s":
                dNighborX = int(np.random.normal(0., contactsWidthSick))
                dNighborY = int(np.random.normal(0., contactsWidthSick))
            else:
                dNighborX = int(np.random.normal(0., contactsWidth))
                dNighborY = int(np.random.normal(0., contactsWidth))

        nighborX = self.ix + dNighborX
        nighborY = self.iy + dNighborY
#         print(nighborX, nighborY)

#        boundary conditions

#         # wrap around 
#         if nighborX >= size:
#             nighborX = nighborX - size
#         if nighborX < 0:
#             nighborX = size + nighborX
#             
#         if nighborY >= size:
#             nighborY = nighborY - size
#         if nighborY < 0:
#             nighborY = size + nighborY

#        try again if exited (seek anyway for contacts even if at the wall)
#        ?????

#       invert (seeking for contacts anyway)
        if nighborX >= size:
            nighborX = self.ix - dNighborX
        if nighborX < 0:
            nighborX = self.ix - dNighborX
             
        if nighborY >= size:
            nighborY = self.iy - dNighborY
        if nighborY < 0:
            nighborY = self.iy - dNighborY
        
        return nighborX, nighborY

    def __init__(self, ix=None, iy=None):
        
        self.ix = ix
        self.iy = iy
        self.state = State.h  # healthy, infected, Infectious, sick, resistant, 
        self.t = {'h':0, 'i':0, 'I':0, 's':0, 'r':0, }  # log days of state changes


def concatImg(im1, im2):
    dst = Image.new('RGB', (im1.width + im2.width, min(im1.height, im2.height)))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    return dst


def mkMovie(name="movie"):
    os.chdir(sDir)
    
    sCmd = "ffmpeg -framerate " + str(framerate) + " -i \"img%03d.png\" -y " + name + "m.mp4"
    print(sCmd)
    os.system(sCmd)
    
    imgP = Image.open(name + "p.png")
    imgLP = concatImg(imgLegend, imgP)
    imgLP.save(name + "lp.png")
    sCmd = "ffmpeg -loop 1 -f image2 -i " + name + "lp.png -c:v libx264 -t 8 -y " + name + "lp.mp4"
    print(sCmd)
    os.system(sCmd)

    sInfoFile = "info.png"
    im = Image.open(sInfoFile)
    im = im.resize((int(movieSize * 1.5), movieSize), Image.ANTIALIAS)
    im.save(sInfoFile)
    sCmd = "ffmpeg -loop 1 -f image2 -i info.png -c:v libx264 -t 8 -y " + name + "i.mp4"
    os.system(sCmd)
    
    sCmd = "ffmpeg -i " + name + "m.mp4 -i " + name + "lp.mp4 -i " + name + "i.mp4 -filter_complex \"[0:v:0] [1:v:0] [2:v:0] concat=n=3:v=1 [v]\" -map \"[v]\" -y " + name + ".mp4"
    print(sCmd)
    os.system(sCmd)
    
    sCmd = "ffmpeg -i " + name + ".mp4 -c:v libx264 -preset slow -crf 18 -c:a copy -pix_fmt yuv420p -y " + name + ".mkv"
    print(sCmd)
    os.system(sCmd)

# working on further details not necessary in this rough model
# class PersonType():
# 
#     def __init__(self, contactsWidth=5, contactsFrq=3, infectionProbability_I=3):
#         self.contactsWidth = contactsWidth 
#         self.contactsFrq = contactsFrq
#         self.infectionProbability_I = infProb_I

    
if __name__ == '__main__':
    e = Corona()
    e.run()
#    mkMovie("04-10_22-37")
 
