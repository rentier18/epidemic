# Simplified modeling of corona epidemic
**Just to get an idea of the dynamics**

<img src="plot.png" width="300"><img src="snapshot.png" width="300">

* Population is in a square grid.

* Process:
    * everyone contacts, daily, random people in the neighborhood
    * in random distance in the grid; normally distributed, rounded
    * Infection occurs at every n-th contact with an infectious person
        
* States / Sequence of disease:
    * healthy,
    * infected, incubation (3 days),
    * infectious, unnoticed, contacts like healthy (3 days),
    * sick (and infectious; less critical, because careful handling) (6 days),
    * recovered, immune (after 12 days)

